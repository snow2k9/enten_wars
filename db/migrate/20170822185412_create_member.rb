class CreateMember < ActiveRecord::Migration[5.1]
  def change
    create_table :members do |t|
      t.string :gw_username
      t.string :gw_token
    end
  end
end
