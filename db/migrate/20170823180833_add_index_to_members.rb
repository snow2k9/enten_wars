class AddIndexToMembers < ActiveRecord::Migration[5.1]
  def change
    add_index(:members, :gw_username, unique: true)
  end
end
