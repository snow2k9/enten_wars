# Enten Wars Guild Overview

## Environment Variables

### `GWAPIKEY`
- Create one [here](https://account.arena.net/applications)
- Needs Guildleader rights


### `GUILD_ID`
- Extracted from https://api.guildwars2.com/v2/account
- Or from https://api.guildwars2.com/v1/guild_details.json?guild_name=enten%20wars