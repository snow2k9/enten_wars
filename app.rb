require 'sinatra'
require 'sinatra/content_for'
require 'sinatra/activerecord'
require 'faraday'
require 'json'

require './config/environment.rb'
require './models/member.rb'
require './models/post.rb'

def guildwars
  conn = Faraday.new(:url => 'https://api.guildwars2.com')
  conn.authorization :Bearer, ENV['GWAPIKEY']
  # conn.response :logger
  conn
end

class EntenWars < Sinatra::Base
  helpers Sinatra::ContentFor
  register Sinatra::ActiveRecordExtension

  before do
    @Guild = guildInfo
  end

  get "/" do
    @DailyIDs = getDailies
    @Dailies = getDailyDetails(@DailyIDs)
    @Posts = Post.all.order('created_at DESC')
    erb :index
  end

  get "/posts/:id" do
    @Posts = Post.find(params['id'])
    erb :post
  end

  get "/guild" do
    erb :guild
  end

  get "/guild/treasury" do
    itemIDs = Array.new
    upgradeIDs = Array.new

    response = guildwars.get '/v2/guild/'+ENV['GUILD_ID']+'/treasury'
    @Treasury = JSON.parse(response.body)

    for needed in @Treasury
      itemIDs << needed['item_id']
      for upgrade in needed['needed_by']
        upgradeIDs << upgrade['upgrade_id']
      end
    end

    @Upgrades = getUpgradeDetails(upgradeIDs.uniq.join(','))
    @Items = getItemDetails(itemIDs.uniq.join(','))

    erb :treasury
  end

  get "/guild/upgrades" do
    erb :upgrades
  end

  get "/guild/members" do
    enten = Member.all
    @Members = getMembers

    @Members.each do |member|
      ente = enten.select { |e| e.gw_username == member['name']}.first
      if ente
        (ente.gw_token)? member['characters'] = getCharacters(ente.gw_token) : nil
        (ente.nickname)? member['nickname'] = ente.nickname : nil
        (ente.id)? member['int_id'] = ente.id : nil
      end
    end

    erb :members
  end

  get "/members/:member/:character" do
    member = Member.find(params['member'])
    @Character = getCharacter(member.gw_token, params['character'])

    erb :character
  end

  # Helpers
  def guildInfo
    response = guildwars.get '/v2/guild/'+ENV['GUILD_ID']
    JSON.parse(response.body, object_class: OpenStruct)
  end

  def getDailies
    response = guildwars.get '/v2/achievements/daily'
    JSON.parse(response.body)
  end

  def getAchievementDetails(dailies)
    response = guildwars.get '/v2/achievements', ids: dailies, lang: 'de'
    JSON.parse(response.body)
  end

  def getDailyDetails(dailies)
    dailyIDs = Array.new
    # dailies = getDailies

    for key in dailies.keys
      for daily in dailies[key]
        dailyIDs << daily['id']
      end
    end

    getAchievementDetails(dailyIDs.uniq.join(','))
  end

  def getMembers
    response = guildwars.get '/v2/guild/'+ENV['GUILD_ID']+'/members'
    JSON.parse(response.body)
  end

  def getUpgrades
    response = guildwars.get '/v2/guild/'+ENV['GUILD_ID']+'/upgrades'
    JSON.parse(response.body)
  end

  def getItemDetails(itemIds)
    response = guildwars.get '/v2/items', ids: itemIds, lang: 'de'
    JSON.parse(response.body)
  end

  def getUpgradeDetails(upgradeIds)
    response = guildwars.get '/v2/guild/upgrades', ids: upgradeIds, lang: 'de'
    upgrades = JSON.parse(response.body)
  end

  def getCharacter(token, character)
    char = Hash.new
    character = URI.escape(character)
    conn = Faraday.new(:url => 'https://api.guildwars2.com')
    conn.authorization :Bearer, token

    response = conn.get '/v2/characters/'+character+"/core", lang: 'de'
    char[:core] = JSON.parse(response.body)

    # response = conn.get '/v2/characters/'+character+"/extras", lang: 'de'
    # char[:extras] = JSON.parse(response.body)

    # response = conn.get '/v2/characters/'+character+"/backstory", lang: 'de'
    # char[:story] = JSON.parse(response.body)

    response = conn.get '/v2/professions/'+char[:core]['profession']
    char[:icon] = JSON.parse(response.body)['icon_big']

    char
  end

  def getCharacters(token)
    conn = Faraday.new(:url => 'https://api.guildwars2.com')
    conn.authorization :Bearer, token
    response = conn.get '/v2/characters'
    JSON.parse(response.body)
  end
end